﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.demo
{
    public class ProductDemo 
    {
        public Product createProductTest(int id, string name, int categoryId)
        {
            Product product = new Product(id, name, categoryId);
            return product;
        }
        public void printProduct(List<Product> products)
        {
            for (int i = 0; i < products.Count; i++)
            {
                Console.WriteLine("Sản phẩm thứ " + i);
                Console.WriteLine("Mã sản phẩm" + products[i].id);
                Console.WriteLine("Tên sản phẩm" + products[i].name);
                Console.WriteLine("Mã loại sản phẩm" + products[i].categoryId);
                Console.WriteLine();
            }
        }
    }
}
