﻿using OOP_SonVV.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.demo
{
    public class DatabaseDemo
    {
        Database database = new Database();
        public void insertTableTest(string name,dynamic row)
        {                 
            database.insertTable(name, row);
        }
        public void selectTableTest(string name, dynamic row)
        {
            database.selectTable(name, row);
        }
        public void updateTableTest(string name, dynamic row)
        {
            database.updateTable(name, row);
        }
        public void deleteTableTest(string name, dynamic row)
        {
            database.deleteTable(name, row);
        }
        public void trunceTableTest(string name)
        {
            database.trunceTable(name);
        }
        public void initTableTest(string name, dynamic row)
        {
            for (int i = 0; i < 10; i++)
            {
                database.insertTable(name, row);
            }        
        }
        public void printTableTest(string name)
        {
            for (int i = 0; i < 10; i++)
            {
                database.show(name);
            }
        }
        public void updateTableTest(int id,dynamic row)
        {
            database.updateTableById(id, row);
        }
    }
}
