﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.dao
{
    public class Database : BaseRow
    {
        public List<Product> productTable = new List<Product>();
        public List<Category> categoryTable = new List<Category>();
        public List<Accessory> accessoryTable = new List<Accessory>();
        public Database instants;
        public delegate List<Object> DatabaseDelegate(string rowName);
        public void show(string name)
        {
            string nameProductTable = "Product";
            string nameCategoryTable = "Category";
            string nameAccessoryTable = "Accessory";
            if (name == nameProductTable)
            {
                for (int i = 0; i < productTable.Count; i++)
                {
                    Console.WriteLine("Thông tin sản phẩm");
                    if (productTable[i].GetType() == typeof(Product))
                    {
                        var product = (Product)productTable[i];
                        Console.WriteLine(product.name);
                    }
                }
            }
            else if (name == nameCategoryTable)
            {
                for (int i = 0; i < productTable.Count; i++)
                {
                    Console.WriteLine("Thông tin sản phẩm");
                    if (categoryTable[i].GetType() == typeof(Category))
                    {
                        var category = (Category)categoryTable[i];
                        Console.WriteLine(category.name);
                    }
                }
            }
            else
            {
                for (int i = 0; i < accessoryTable.Count; i++)
                {
                    Console.WriteLine("Thông tin sản phẩm");
                    if (accessoryTable[i].GetType() == typeof(Accessory))
                    {
                        var accessory = (Accessory)accessoryTable[i];
                        Console.WriteLine(accessory.name);
                    }
                }
            }
        }
        public int insertTable(string name, dynamic row)
        {
            if (name == "Product" && row.GetType() == typeof(Product))
            {
                productTable.Add(row);

                return 1;
            }
            else if (name == "Category" && row.GetType() == typeof(Category))
            {
                categoryTable.Add(row);
                return 2;
            }
            else if (name == "Accessory" && row.GetType() == typeof(Accessory))
            {
                accessoryTable.Add(row);
                return 3;
            }
            return 0;
        }

        public dynamic selectTable(string name, string where)

        {
            if (name.Equals("Product"))
            {
                var product = productTable.Where(x => x.name == where).ToList();
                return product;
            }
            else if (name.Equals("Category"))
            {
                return categoryTable.Where(x => x.name.Equals(where)).ToList();
            }
            return categoryTable.Where(x => x.name.Equals(where)).ToList();
            //if (name == "Product")
            //{
            //    List<Product> data = new List<Product>();
            //    data = productTable.Where(x => x.name == where).ToList();
            //    Reponse reponseProduct = new Reponse();
            //    reponseProduct.products = data;
            //    return reponseProduct;
            //}
            //else if (name == "Category")
            //{
            //    List<Category> data = new List<Category>();
            //    data = categoryTable.Where(x => x.name == where).ToList();
            //    Reponse reponseCategory = new Reponse();
            //    reponseCategory.category = data;
            //    return reponseCategory;
            //}
            //List<Accessory> dataAccessory = new List<Accessory>();
            //dataAccessory = accessoryTable.Where(x => x.name == where).ToList();
            //Reponse reponse = new Reponse();
            //reponse.accessory = dataAccessory;
            //return reponse;
            //selectTable("Product",((x)=> { return x.name == "Product"}))
        }
        public int updateTable(string name, dynamic row)
        {
            if (name.Equals("Product"))
            {
                for (int i = 0; i < productTable.Count; i++)
                {
                    row = productTable[i].GetType();
                    if (productTable[i].id == row.id)
                    {
                        productTable[i] = row;
                    }
                }
                return 1;
            }
            else if (name.Equals("Category"))
            {
                if (name.Equals("Category"))
                {
                    for (int i = 0; i < categoryTable.Count; i++)
                    {
                        row = categoryTable[i].GetType();
                        if (categoryTable[i].id == row.id)
                        {
                            categoryTable[i] = row;
                        }
                    }
                    return 2;
                }
            }
            else if (name.Equals("Accessory"))
            {
                for (int i = 0; i < categoryTable.Count; i++)
                {
                    row = categoryTable[i].GetType();
                    if (categoryTable[i].id == row.id)
                    {
                        categoryTable[i] = row;
                    }
                }
                return 3;
            }
            return 0;
        }
        public Boolean deleteTable(string name, dynamic row)

        {
            if (name.Equals("Product"))
            {
                for (int i = 0; i < productTable.Count; i++)
                {
                    row = productTable[i].GetType();
                    if (productTable[i].id == row.id)
                    {
                        productTable.Remove(productTable[i]);
                        return true;
                    }
                    return false;
                }
            }
            else if (name.Equals("Category"))
            {
                if (name.Equals("Category"))
                {
                    for (int i = 0; i < categoryTable.Count; i++)
                    {
                        row = categoryTable[i].GetType();
                        if (categoryTable[i].id == row.id)
                        {
                            categoryTable.Remove(categoryTable[i]);
                            return true;
                        }
                        return false;
                    }
                }
            }
            else if (name.Equals("Accessory"))
            {
                for (int i = 0; i < accessoryTable.Count; i++)
                {
                    row = accessoryTable[i].GetType();
                    if (accessoryTable[i].id == row.id)
                    {
                        accessoryTable.Remove(accessoryTable[i]);
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }
        public void trunceTable(string name)
        {
            if (name.Equals("Product"))
            {
                productTable.RemoveAll(product => product.id != null);
                Console.WriteLine("xóa hết product");
            }
            else if (name.Equals("Category"))
            {
                categoryTable.RemoveAll(category => category.id != null);
                Console.WriteLine("xóa hết category");
            }
            else if (name.Equals("Accessory"))
            {
                categoryTable.RemoveAll(accessoryTable => accessoryTable.id != null);
                Console.WriteLine("xóa hêt accessory");
            }
        }
        public int updateTableById(int id, dynamic row)
        {
            if (row.GetType() == productTable[0].GetType())
            {
                row = productTable[0].GetType();
                for (int i = 0; i < productTable.Count; i++)
                {
                    if (productTable[i].id == id)
                    {
                        row = productTable[i];
                        return 1;
                    }
                    return 0;
                }
            }
            else if (row.GetType() == categoryTable[0].GetType())
            {
                row = categoryTable[0].GetType();
                for (int i = 0; i < categoryTable.Count; i++)
                {
                    if (categoryTable[i].id == id)
                    {
                        row = categoryTable[i];
                        return 1;
                    }
                    return 0;
                }
            }
            else if (row.GetType() == accessoryTable[0].GetType())
            {
                row = accessoryTable[0].GetType();
                for (int i = 0; i < accessoryTable.Count; i++)
                {
                    if (accessoryTable[i].id == id)
                    {
                        row = accessoryTable[i];
                        return 1;
                    }
                    return 0;
                }
            }
            return 0;
        }
    }
}

