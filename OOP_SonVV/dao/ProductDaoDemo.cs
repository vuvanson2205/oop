﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.dao
{
    public class ProductDaoDemo
    {
        ProdcutDao prodcutDao = new ProdcutDao();
        public bool insertProductTest(Product product)         
        {
            if (product != null)
            {
                prodcutDao.insert(product);
                return true;
            }
            return false;
        }
        public Boolean findProductAllTest(Product product)
        {
            if (product != null)
            {
                prodcutDao.findAllTest(product);
                return true;
            }
            return false;
        }
        public Boolean updateProductTest(Product product)
        {
            if (product != null)
            {
                prodcutDao.updateTest(product);
                return true;
            }
            return false;
        }
        public Boolean deleteProductTest(Product product)
        {
            if (product != null)
            {
                prodcutDao.deleteTest(product);
                return true;
            }
            return false;
        }
        public Boolean trunceProductTest(string productName)
        {
            if (productName != null && productName =="Product")
            {
                prodcutDao.trunceTest(productName);
                return true;
            }
            return false;
        }
    }
}
