﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.dao
{
    public class CategoryDao
    {
        Database database = new Database();
        public Boolean  insert(object row)
        {
            if (row == database.categoryTable[0].GetType())
            {
                string category = "Category";
                database.insertTable(category, row);
                return true;
            }
            return false;
        }
        public Boolean findAllTest(dynamic row)
        {
            if (row == database.categoryTable[0].GetType())
            {
                string category = "Category";
                database.selectTable(category, row);
                return true;
            }
            return false;
        }
        public Boolean updateTest(dynamic row)
        {
            if (row == database.categoryTable[0].GetType())
            {
                string category = "Category";
                database.updateTable(category, row);
                return true;
            }
            return false;
        }
        public Boolean deleteTest(dynamic row)
        {
            if (row == database.categoryTable[0].GetType())
            {
                string category = "Category";
                database.deleteTable(category, row);
                return true;
            }
            return false;
        }
        public Boolean trunceTest(dynamic row)
        {
            if (row == database.categoryTable[0].GetType())
            {
                string category = "Category";
                database.trunceTable(category);
                return true;
            }
            return false;
        }
    }
}
