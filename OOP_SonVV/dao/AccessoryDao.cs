﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.dao
{
    public class AccessoryDao
    {
        Database database = new Database();
        public int insertAccessoryTest(string accessoryName,Accessory accessory)
        {
            accessoryName = "Accessory";
            if (accessory != null)
            {
                database.insertTable(accessoryName, accessory);
                return 1;
            }
            return 0;
        }
        public List<Accessory> selectAccessoryTest(string accessory, string accessoryName)
        {
            List<Accessory> accessories = new List<Accessory>();     
            accessoryName = "Accessory";
            Accessory accessoryRoot = new Accessory();
            if (accessoryName != null)
            {
                for (int i = 0; i < database.accessoryTable.Count; i++)
                {
                    accessoryRoot = database.accessoryTable[i];
                    if (accessoryRoot.name == accessoryName)
                    {
                        accessories.Add(accessoryRoot);
                    }
                }
                return accessories;
            }
            return accessories;    
        }
        public int updateAccessoryTest(Accessory accessory)
        {
            string accessoryName = "Accessory";
            if (accessory != null)
            {
                database.updateTable(accessoryName, accessory);
                return 1;
            }
            return 0;   
        }
        public Boolean deleteAccessoryTest(Accessory accessory)
        {
            string accessoryName = "Accessory";
            if(accessory != null)
            {
                database.deleteTable(accessoryName, accessory);
                return true;
            }  
            return false;
        }
        public void trunceAccessoryTest(string  accessoryName)
        {
           accessoryName = "Accessory";
            database.trunceTable(accessoryName);    
        }
    }
}
