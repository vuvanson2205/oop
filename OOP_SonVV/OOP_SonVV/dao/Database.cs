﻿using OOP_SonVV.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.dao
{
    public class Database : Base, IModel
    {
        public List<object> productTable = new List<object>();
        public List<object> categoryTable = new List<object>();
        public List<object> accessoryTable = new List<object>();
        public Database instants;
        public void show()
        {
            for (int i = 0; i < productTable.Count; i++)
            {
                Console.WriteLine(productTable[i]);
            }
        }
        
        public int insertTable(string name, object row)
        {
            if(name == "Product") {
               
               productTable.Add(row);
                return 1;
            }
            else if(name == "Category")
            {
                categoryTable.Add(row);
                return 2;
            }
            accessoryTable.Add(row);
            return 3;
            
        }
    }
}
