﻿using OOP_SonVV.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.entity
{
    public class Accessory :Base, IModel
    {
      public  Accessory() { }
        public Accessory(int id ,string name)
        {
              this.id = id;
            this.name = name;   
        }
        public void setId(int id)
        {
            this.id = id;
        }
        public int getId()
        {
            return id;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public string getName() { return name; }
    }
}
