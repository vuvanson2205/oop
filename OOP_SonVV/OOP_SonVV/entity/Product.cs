﻿using OOP_SonVV.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.entity 
{
    public class Product:Base, IModel
    {
        public Product()
        {

        }
        public Product(int id, string name, int categoryId)
        {
            this.id = id;
            this.name = name;
            this.categoryId = categoryId;
        }
        //lấy ra các thuộc tính
        public int getId()
        {
            return id;
        }
        public string getName()
        {
            return name;
        }
        public int getCategoryId()
        {
            return categoryId;
        }
    }
}
