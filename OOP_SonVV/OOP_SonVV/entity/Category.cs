﻿using OOP_SonVV.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.entity
{
    public class Category :Base, IModel
    {
        
        public Category()
        {

        }
        public Category(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
        public int getId()
        {
            return id;
        }
        public string getName()
        {
            return name;
        }
        public void setId(int id)
        {
            this.id = id;
        }
        public void setName(string Name)
        {
            this.name = Name;
        }
    }
}
