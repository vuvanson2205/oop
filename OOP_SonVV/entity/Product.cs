﻿using OOP_SonVV.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_SonVV.entity
{
    public class Product : BaseRow, IModel
    {
        public int categoryId { get; set; }
        public override string ToString()
        {
            return this.id + " " + this.name + " " + this.categoryId;
        }
        public Product()
        {

        }
        public Product(int id,string name,int categoryId)
        {
            this.id = id;
            this.name = name;   
            this.categoryId = categoryId;   
        }
    }
}
